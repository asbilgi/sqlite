/**
 * Copyright(c) 2006-2010 
 * KADE INFORMATION TECHNOLOGIES AND SECURITY SYSTEMS LTD.
 * http://www.kade.com.tr 
 *
 * Filename         : DbException.cpp
 * Author           : Volkan SALMA
 * Date             : 03.11.2010
 * Description      : Definition file for class DbException.
 *
 */

// Precompiled header

// Standard includes

// Third Party includes

// Project includes
#include "DbException.h"

// Using directives
using namespace kade;

// Class Functions


DbException::DbException(void)
{
}


DbException::~DbException(void)
{
}

std::string DbException::getExceptionText() const
{
    std::string msg;
	switch(m_errorCode)
	{
		    case DbException::CELL_CONTENT_IS_NULL:
			      return "CELL_CONTENT_IS_NULL";
		    case DbException::CELL_OUTPUT_TYPE_NOT_SUPPORTED:
			      return "CELL_OUTPUT_TYPE_NOT_SUPPORTED";

        case DbException::CELL_INDEX_OUT_OF_BOUNDS:
			      return "CELL_INDEX_OUT_OF_BOUNDS";

        case DbException::ROW_INDEX_OUT_OF_BOUNDS:
			      return "ROW_INDEX_OUT_OF_BOUNDS";

        case DbException::COLUMN_COUNT_ON_ROW_ISNT_AVAILABLE:
            return "COLUMN_COUNT_ON_ROW_ISNT_AVAILABLE";

        case DbException::DB_SPECIFIC_EXCEPTION:
            return m_dbSpecificText;

        case DbException::CONNECTION_COULD_NOT_ESTABLISHED:
            msg = "CONNECTION_COULD_NOT_ESTABLISHED\n"; 
            msg += m_dbSpecificText;
            return msg;

        case DbException::DBNAME_COULD_NOT_SET_AS_SCHEMA:
            msg = "DBNAME_COULD_NOT_SET_AS_SCHEMA\n";
            msg += m_dbSpecificText;
            return msg;

		    case DbException::TABLE_CHECK_OPERATION_FAILED:
			    msg = "TABLE_CHECK_OPERATION_FAILED\n";
			    return msg;

		    case DbException::REPAIR_TABLE_REQUEST_COULD_NOT_EXECUTED:
			    msg = "REPAIR_TABLE_REQUEST_COULD_NOT_EXECUTED\n";
			    return msg;

		    case DbException::MAX_CONNECTION_REQUEST_COULD_NOT_EXECUTED:
			    msg = "MAX_CONNECTION_REQUEST_COULD_NOT_EXECUTED\n";
			    return msg;

		    case DbException::QUERY_FOR_RESULTSET_COULD_NOT_EXECUTED:
			    msg = "QUERY_FOR_RESULTSET_COULD_NOT_EXECUTED\n";
			    return msg;

		    case DbException::NON_QUERY_REQUEST_COULD_NOT_EXECUTED:
			    msg = "NON_QUERY_REQUEST_COULD_NOT_EXECUTED\n";
			    return msg;

		    default:
			    return "Undefined exception id.";
	}
}

//Copy Constructor.
DbException::DbException(const DbException &rhs)
{
    m_errorCode = rhs.m_errorCode;
    m_dbSpecificText = rhs.m_dbSpecificText;
}

//Assigment Operator.
DbException & DbException::operator=(const DbException &rhs)
{
    return *this;
}