#include "SQLiteDB.h"
#include <iostream>
#include <stdlib.h>
#include <sstream>


SQLiteDB::SQLiteDB()
{
    //dbFile[0] = NULL;
}


SQLiteDB::~SQLiteDB()
{
    db.close ();
}

void SQLiteDB::SetDBFile( std::string dbFile )
{
    db.close();
    //sprintf ( this->dbFile, "%s", dbFile );
    db.open ( dbFile.c_str() );
}

void SQLiteDB::CloseDB()
{
    db.close();
}

bool SQLiteDB::executeQuery(std::string query)
{
    int result = db.execDML( query );
    bool isSuccess = true;

    if(result == -11)
        isSuccess = false;
    
    return isSuccess;
}

std::vector< std::vector<std::string> > SQLiteDB::executeQueryWithReturnValues(std::string query)
{
    std::vector< std::vector<std::string> > queryResult;
    CppSQLite3Query q = db.execQuery(query.c_str());
    
    for(int i=0;!q.eof();i++)
    {
        std::vector<std::string> tempVec;
        for(int j = 0; j < q.numFields(); j++)
        {
            tempVec.push_back(q.fieldValue(j));
        }
        queryResult.push_back(tempVec);
        q.nextRow();
    }

    return queryResult;
}


