#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Release configuration
CND_PLATFORM_Release=JessieCrossCompiler-Linux
CND_ARTIFACT_DIR_Release=dist/Release/JessieCrossCompiler-Linux
CND_ARTIFACT_NAME_Release=libsqlite.so
CND_ARTIFACT_PATH_Release=dist/Release/JessieCrossCompiler-Linux/libsqlite.so
CND_PACKAGE_DIR_Release=dist/Release/JessieCrossCompiler-Linux/package
CND_PACKAGE_NAME_Release=libSQLiteLibrary.so.tar
CND_PACKAGE_PATH_Release=dist/Release/JessieCrossCompiler-Linux/package/libSQLiteLibrary.so.tar
# ReleaseUbuntu16.04 configuration
CND_PLATFORM_ReleaseUbuntu16.04=GNU-Linux
CND_ARTIFACT_DIR_ReleaseUbuntu16.04=dist/ReleaseUbuntu16.04/GNU-Linux
CND_ARTIFACT_NAME_ReleaseUbuntu16.04=libsqlite.so
CND_ARTIFACT_PATH_ReleaseUbuntu16.04=dist/ReleaseUbuntu16.04/GNU-Linux/libsqlite.so
CND_PACKAGE_DIR_ReleaseUbuntu16.04=dist/ReleaseUbuntu16.04/GNU-Linux/package
CND_PACKAGE_NAME_ReleaseUbuntu16.04=libSQLiteLibrary.so.tar
CND_PACKAGE_PATH_ReleaseUbuntu16.04=dist/ReleaseUbuntu16.04/GNU-Linux/package/libSQLiteLibrary.so.tar
# Debug configuration
CND_PLATFORM_Debug=JessieCrossCompiler-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/JessieCrossCompiler-Linux
CND_ARTIFACT_NAME_Debug=libSQLiteLibrary.so
CND_ARTIFACT_PATH_Debug=dist/Debug/JessieCrossCompiler-Linux/libSQLiteLibrary.so
CND_PACKAGE_DIR_Debug=dist/Debug/JessieCrossCompiler-Linux/package
CND_PACKAGE_NAME_Debug=libSQLiteLibrary.so.tar
CND_PACKAGE_PATH_Debug=dist/Debug/JessieCrossCompiler-Linux/package/libSQLiteLibrary.so.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
