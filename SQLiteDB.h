#ifndef SQLITEDB_H
#define SQLITEDB_H

#include "CppSQLite3.h"
#include "DbException.h"
#include <ctime>
#include <string>
#include <vector>
#include <sstream>
#include <typeinfo>


class SQLiteDB
{
private:
    //char dbFile[512];
    
public:
#ifdef _WIN32
    __declspec(dllexport) SQLiteDB();
    __declspec(dllexport) ~SQLiteDB();
    __declspec(dllexport) bool executeQuery(std::string query);
    template <class T>
    __declspec(dllexport) T executeQueryWithReturnValue(std::string query);
    template <class T>
    __declspec(dllexport) T getValue(std::vector< std::vector<std::string> >,int rowIndex,int columnIndex);
    __declspec(dllexport) std::vector< std::vector<std::string> > executeQueryWithReturnValues(std::string query);
    __declspec(dllexport) void CloseDB();
    __declspec(dllexport) void SetDBFile( std::string dbFile );
#else
    SQLiteDB();
    ~SQLiteDB();
    bool executeQuery(std::string query);
    template <class T>
    T executeQueryWithReturnValue(std::string query);
    template <class T>
    T getValue(std::vector< std::vector<std::string> >,int rowIndex,int columnIndex);
    std::vector< std::vector<std::string> > executeQueryWithReturnValues(std::string query);
    void CloseDB();
    void SetDBFile( std::string dbFile );
#endif
    CppSQLite3DB db;
};

template <class T>
T SQLiteDB::getValue(std::vector< std::vector<std::string> > dataVec,int rowIndex,int columnIndex)
{
    T result;

    std::istringstream out(dataVec[rowIndex][columnIndex]);
    if( typeid( T ) == typeid( std::string ) )
		{
        std::string TempResult;
			  std::getline( out, TempResult );
			  result = reinterpret_cast< T & >( TempResult );
		}
    else 
    {
        if( !(out >> result) ) 
			  {
            throw kade::DbException(kade::DbException::CELL_OUTPUT_TYPE_NOT_SUPPORTED);
			  }
    }

    return result;
}

template <class T>
T SQLiteDB::executeQueryWithReturnValue(std::string query)
{
    T result;
    
    CppSQLite3Query q = db.execQuery(query.c_str());
    std::istringstream out(q.getStringField(0));
    if(!q.eof())
    {
        if( typeid( T ) == typeid( std::string ) )
		    {
            std::string TempResult;
			      std::getline( out, TempResult );
			      result = reinterpret_cast< T & >( TempResult );
		    }
        else 
        {
            if( !(out >> result) ) 
			      {
				      throw kade::DbException(kade::DbException::CELL_OUTPUT_TYPE_NOT_SUPPORTED);
			      }
        }
    }
    else 
        throw kade::DbException(kade::DbException::CELL_CONTENT_IS_NULL);

    return result;
}

#endif //define SQLITEDB_H
