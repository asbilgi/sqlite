#include "SQLiteDB.h"
#include <iostream>

SQLiteDB db;

bool CreateHumanCountTable()
{
    bool isSuccess;
    std::stringstream ss;
    ss << "CREATE TABLE IF NOT EXISTS humancount ("
        << "auto_inc INTEGER PRIMARY KEY,"
        << "IsSent int(10) NOT NULL DEFAULT '0',"
        << "RecordID varchar(100)  NOT NULL,"
        << "DeviceID varchar(30) NOT NULL,"
        << "CustomerID varchar(30) NOT NULL,"
        << "MeasurementStartTime datetime NOT NULL,"
        << "MeasurementEndTime datetime NOT NULL,"
        << "IncomingHumanCount int(11) NOT NULL,"
        << "OutgoingHumanCount int(11) NOT NULL,"
        << "UNIQUE (RecordID)"
        << ")";
     
    isSuccess = db.executeQuery(ss.str());

    return isSuccess;
}

bool InsertHumanCountEntry(bool isSent,std::string recordID ,int deviceID,int customerID, std::string measurementStartTime ,std::string measurementEndTime,int incoming,int outgoing)
{
    bool isSuccess;
    std::stringstream ss;

    ss << "INSERT INTO humancount (auto_inc, IsSent, RecordID,DeviceID,CustomerID,MeasurementStartTime, MeasurementEndTime, IncomingHumanCount, OutgoingHumanCount ) values (NULL,"
        << isSent << ", "
        << "'" << recordID << "'" << ", "
        << deviceID << ", "
        << customerID << ", "
        << "'" << measurementStartTime << "'" << ", "
        << "'" << measurementEndTime << "'" << ", "
        << incoming << ", "
        << outgoing << ")";

    isSuccess = db.executeQuery(ss.str());

    return isSuccess;
}

int main()
{
  
  db.SetDBFile("vcount.db");

  if(CreateHumanCountTable())
  {
    if(InsertHumanCountEntry(0,"dsadsa-120-dsadsa",1,11,"2012-10-10 10:11:00","2012-10-10 10:11:59",11,21))
        if(InsertHumanCountEntry(0,"dsadsa-121-dsadsa",2,12,"2012-10-10 10:12:00","2012-10-10 10:12:59",12,22))
            if(InsertHumanCountEntry(0,"dsadsa-122-dsadsa",3,13,"2012-10-10 10:13:00","2012-10-10 10:13:59",13,23))
                if(InsertHumanCountEntry(0,"dsadsa-123-dsadsa",4,14,"2012-10-10 10:14:00","2012-10-10 10:14:59",14,24))
                    if(InsertHumanCountEntry(0,"dsadsa-124-dsadsa",5,15,"2012-10-10 10:15:00","2012-10-10 10:15:59",15,25))
                         std::cout<<"OK"<<std::endl;
  }
  
  std::vector< std::vector<std::string> > vec = db.executeQueryWithReturnValues("select * from humancount where auto_inc>0");
  for(int i = 0;i<vec.size();i++)
  {
      for(int j=0;j<vec[i].size();j++)
      {
          std::cout<<vec[i][j]<<" ";
      }
      std::cout<<std::endl;
  }
  try
  {
      
      std::cout<<db.getValue<int>(vec,1,3)+1000<<std::endl;
      std::cout<<db.getValue<std::string>(vec,2,2).append("...")<<std::endl;
        
      int var = db.executeQueryWithReturnValue< int >("select * from humancount where auto_inc=1");
      std::cout<<"--"<<var+100<<"--"<<std::endl;
  }
  catch( kade::DbException & rException )
  {
      std::cerr << "ERROR " << rException.getErrorNum() << " Text: " << rException.getExceptionText() << std::endl;
  }

  db.CloseDB();
  
  return 0;
}
