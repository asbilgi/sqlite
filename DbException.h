/**
 * Copyright(c) 2006-2010 
 * KADE INFORMATION TECHNOLOGIES AND SECURITY SYSTEMS LTD.
 * http://www.kade.com.tr 
 *
 * Filename         : DbException.h
 * Author           : Volkan SALMA
 * Date             : 03.11.2010
 * Description      : Exception class for handling db operations exceptions.         
 *
 */

#ifndef DB_EXCEPTION_H
#define DB_EXCEPTION_H

#include <sstream>
#include <string>

#ifdef _MSC_VER
    #define DECLARE_THROW(Exception) throw(Exception)
#else
    #define DECLARE_THROW(Exception)
#endif

namespace kade
{
    class DbException
    {
    public:

      enum DBExceptionListType
	    {
            DBNAME_COULD_NOT_SET_AS_SCHEMA,
            QUERY_FOR_RESULTSET_COULD_NOT_EXECUTED,
			      NON_QUERY_REQUEST_COULD_NOT_EXECUTED,
            STATEMENT_COULD_NOT_CREATED,
            CONNECTION_COULD_NOT_ESTABLISHED,
            DB_SPECIFIC_EXCEPTION,
            CELL_CONTENT_IS_NULL,
            CELL_OUTPUT_TYPE_NOT_SUPPORTED,
            CELL_INDEX_OUT_OF_BOUNDS,
            ROW_INDEX_OUT_OF_BOUNDS,
            COLUMN_COUNT_ON_ROW_ISNT_AVAILABLE,
            COLUMN_TYPE_NOT_CONSISTENT,
            NO_ROW_ON_RESULTSET,
			      TABLE_CHECK_OPERATION_FAILED,
			      REPAIR_TABLE_REQUEST_COULD_NOT_EXECUTED,
			      MAX_CONNECTION_REQUEST_COULD_NOT_EXECUTED
            
        };

#ifdef _WIN32
        //Default Constructor
        __declspec(dllexport) DbException(void);

        //Default Destructor.
        __declspec(dllexport) ~DbException(void);

        //Copy Constructor.
        __declspec(dllexport) DbException(const DbException &rhs);

        //Assigment Operator.
        DbException & operator=(const DbException &rhs);

        //Interface.

        __declspec(dllexport) DbException(DBExceptionListType err_,std::string dbSpecificText = "NOT SET.") : m_errorCode(err_),
                                                                           m_dbSpecificText(dbSpecificText){}
	      __declspec(dllexport) DBExceptionListType getErrorNum(void) const {return m_errorCode;}

	      __declspec(dllexport) std::string getExceptionText(void) const;
#else
        //Default Constructor
        DbException(void);

        //Default Destructor.
        ~DbException(void);

        //Copy Constructor.
        DbException(const DbException &rhs);

        //Assigment Operator.
        DbException & operator=(const DbException &rhs);

        //Interface.

        DbException(DBExceptionListType err_,std::string dbSpecificText = "NOT SET.") : m_errorCode(err_),
                                                                           m_dbSpecificText(dbSpecificText){}
	      DBExceptionListType getErrorNum(void) const {return m_errorCode;}

	      std::string getExceptionText(void) const;
#endif
    protected:
    private:
	      DBExceptionListType m_errorCode;
        std::string m_dbSpecificText;
    };
}

#endif /* DB_EXCEPTION_H */